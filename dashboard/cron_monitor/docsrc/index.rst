.. Cron Monitor documentation master file, created by
   sphinx-quickstart on Mon Jul 15 12:37:04 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Cron Monitor's documentation!
========================================

Cron Monitor is both a wrapper script around cron jobs and a Django app that displays information about the job in a web dashboard.

Cron Monitor requires Python 2.7 and a UNIX environment.

Contents:

.. toctree::
   :maxdepth: 2

   intro
   usage
   config

Indices and tables
==================

* :ref:`search`

